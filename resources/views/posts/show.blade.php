@extends('layouts.app')

@section('content')
    <a href="/posts" class="btn btn-default">Go back</a>
    <h1>{{$post->title}}</h1>
    <img style="width:40%" src="/storage/cover_images/{{$post->cover_image}}">
    <div>
        <!-- using !! becouse of article-ckeditor -->
        {!!$post->body!!}
    </div>
    <hr>
    <small>Written by </small> <i>{{$post->user->name}}</i>   <small> on {{$post->created_at}}</small>
    <hr>
    <!-- if guest ... hide edit/delete-->
    @if(!Auth::guest())
        <!-- if user_id == POST/user_id then CRUD-->
        <!-- I added in Users row supermoderator -->
        <!-- Check if user has row (supermoderator) equal to 1 -->
        @if(Auth::user()->id == $post->user_id || Auth::user()->supermoderator == 1 )
    <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit</a>
    {!! Form::open(['action' => ['PostsController@update',  $post->id ], 'method' => 'POST' , 'class' => 'float-right']) !!}
    {{Form::hidden('_method', 'DELETE')}}
    {{Form::submit('Delete', ['class'=> 'btn btn-danger'])}}
    {!!Form::close()!!}
        @endif
    @endif
@endsection