@extends('layouts.app')

@section('content')
    <h1>Posts</h1>
    <!-- Предаваме променливата от контролера и проверяваме дали има нещо в нея-->
    @if(count($posts) > 0 )
        <!-- обхождаме масива с постовете-->
        @foreach ($posts as $post)
            <div class="well border" style="margin-bottom:10px">
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                    <img style="width:50%" src="/storage/cover_images/{{$post->cover_image}}">
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <h3><a href="/posts/{{$post->id}}">{{$post->title}}</a></h3>
                        <small>Written by </small> <i>{{$post->user->name}}</i>   <small> on {{$post->created_at}}</small>
                    </div>
                </div>
            </div>
        @endforeach
    @else
        <p>No posts,Sorry!</p>
    @endif
@endsection