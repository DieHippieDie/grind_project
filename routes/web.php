<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!

Route::get('/users/{id}/{name}', function($id, $name) {
    return 'This is user '.$name.' with an id of '.$id;
});

|
*/

Route::get('/', 'PagesControler@index');

Route::get('/about', 'PagesControler@about');

Route::get('/services', 'PagesControler@services');

//насочваме при подаване на URL -/posts да ни прехвърли към контроелра ,а от там ни прехвърля във view
Route::resource('posts', 'PostsController');


Auth::routes();

Route::get('/dashboard', 'DashboardController@index');
