<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Post extends Model
{
    // Table Name
    protected $table = 'posts';
    // Primary Key
    public $primeryKey = 'id';
    // Timestamps
    public $timestamps = true;

    //правим взамиотношеняита Making relashions between user and post
    public  function user(){
        return $this->belongsTo('App\User');
    }
}
